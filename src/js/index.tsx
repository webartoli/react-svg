import * as React from 'react'  
import * as ReactDOM from 'react-dom'
import App from './app'
import DfmData from './dfm_data'

ReactDOM.render(<App dfmGraph={DfmData} />, document.getElementById('content'))
