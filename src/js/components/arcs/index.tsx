// Arco normale
// Arco ricorsivo
// Arco multiple
// Arco opzionale
// Arco convergente

// Ruolo

import * as React from 'react'
import ReactTooltip from 'react-tooltip'

import { IArc, IGraph, IAttribute } from '../../model/interfaces'
import { IPoint, Point } from '../../model/points'
import { RenderMode } from '../../model/indyco'
import { IStyle } from '../style'
import GraphHelper from '../../helpers/graph-helper'


export default class SvgArc extends React.Component<{ key: string, arc: IArc, style: IStyle, graph: IGraph }, {}> {

  constructor() {
    super()
  }

  render() {

    var { from, to } = new GraphHelper(this.props.graph).getEdgesAttributes(this.props.arc)

    let rolePosition = Point.center(from.position, to.position)

    var line = {
      x1: from.position.x, y1: from.position.y,
      x2: to.position.x, y2: to.position.y
    }

    return <g>
      <line {...line} {...this.props.style.arc} />
      {this.renderRole(rolePosition)}
      {this.decorate(from, to, this.props.arc)}
    </g>
  }

  decorate(from: IAttribute, to: IAttribute, arc: IArc) {

    let center = Point.center(from.position, to.position)
    var line = {
      x1: center.x - 5, y1: center.y,
      x2: center.x + 5, y2: center.y
    }
    return <line {...line} {...this.props.style.arc} />
  }

  renderRole(position: IPoint) {
    if (this.props.arc.role) {
      return <text {...position} {...this.props.style.label} textAnchor="middle" alignmentBaseline="middle">{this.props.arc.role}</text>
    }
    return
  }
}




