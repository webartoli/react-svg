import * as React from 'react'
import SvgArc from './index'
import { IArc, IAttribute } from '../../model/interfaces'

export default class OptionalArc extends SvgArc {

    decorate(from: IAttribute, to: IAttribute, arc: IArc) {
        return null
    }
}
