import * as React from 'react'
import Attribute from './attributes'
import Arc from './arcs'
import * as Dfm from '../model/interfaces'
import { RenderMode } from '../model/indyco'
import { IStyle, StyleManager } from './style'

interface DfmGraphProps {
  width?: number
  height?: number
  graph: Dfm.IGraph
  renderMode: RenderMode
}

export default class DfmGraph extends React.Component<DfmGraphProps, {}> {
  static defaultProps = { width: 800, height: 600 }

  private DEBUG = false

  render() {

    let graph = this.props.graph
    let styleManager = new StyleManager(this.props.renderMode)

    return (
      <svg width={this.props.width} height={this.props.height}>
        <g>
          {graph.arcs.map(a => <Arc key={a.id} arc={a} graph={graph} style={styleManager.getArcStyle(a)} />)}
        </g>
        <g>
          {graph.attributes.map(a => <Attribute key={a.id} attr={a} graph={graph} style={styleManager.getAttibuteStyle(a)} />)}
        </g>
      </svg>
    )
  }
}