import * as React from 'react'
import { RenderMode } from '../model/indyco'
import { IAttribute, IArc } from '../model/interfaces'

export interface IStyle {
  attribute: React.SVGProps
  label: React.SVGProps
  arc: React.SVGProps
  line: React.SVGProps
}

export class StyleManager {

  constructor(private renderMode: RenderMode) {

  }

  private builderStyle(): IStyle {
    return {
      attribute: {
        fill: "white",
        stroke: '#00539C',
        r: 10,
        strokeWidth: 2,
      },
      arc: {
        strokeWidth: 2,
        stroke: '#B4CEDA'
      },
      label: {
        fill: "black"
      },
      line: {
        strokeWidth: 2,
        stroke: "#00539C",
        fill: "transparent",
        strokeLinecap: "round"
      }
    }
  }

  public style(): IStyle {
    if (this.renderMode === RenderMode.Builder)
      return this.builderStyle()

    if (this.renderMode === RenderMode.Explorer)
      return this.builderStyle()
  }

  public getAttibuteStyle(attr: IAttribute): IStyle {
    var style = this.style()
    if (attr.fill) {
      style.attribute.fill = attr.fill
    }
    return style
  }

  public getArcStyle(arc: IArc): IStyle {
    return this.style()
  }
}