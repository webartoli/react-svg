import * as React from 'react'
import Attribute from './attribute'
import { IAttribute } from '../../model/interfaces'

export default class OptionalAttribute extends Attribute {

  decorate(attr: IAttribute) {

    var cutLineWeight = this.props.style.attribute.r as number + 5

    let line = {
      x1: attr.position.x - cutLineWeight,
      y1: attr.position.y,
      x2: attr.position.x + cutLineWeight,
      y2: attr.position.y
    }

    return <line {...line} {...this.props.style.line} />
  }
}
