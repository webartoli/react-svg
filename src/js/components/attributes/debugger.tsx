import * as React from 'react'
import ReactTooltip from 'react-tooltip'

export default class PointDebugger extends React.Component<{ x: number, y: number }, {}> {

  render() {
    let attr = this.props

    const cross = 18
    const r = 1

    let horizontal = {
      x1: attr.x - cross,
      y1: attr.y,
      x2: attr.x + cross,
      y2: attr.y
    }

    let vertical = {
      x1: attr.x,
      y1: attr.y - cross,
      x2: attr.x,
      y2: attr.y + cross
    }

    const lineStyle = {
      strokeWidth: 0.5,
      stroke: "gray",
      fill: "transparent"
    }

    return (
      <g>
        {this.props.children}
        <line {...horizontal } {...lineStyle} />
        <line {...vertical } {...lineStyle} />
        <circle cx={attr.x} cy={attr.y} r={r} fill="#5c0120" />
      </g>
    )
  }
}