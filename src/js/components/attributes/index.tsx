import * as React from 'react'
import ReactTooltip from 'react-tooltip'

import { IAttribute, IGraph } from '../../model/interfaces'
import { RenderMode } from '../../model/indyco'
import { IStyle } from '../style'

import { SvgAttributeProps } from './SvgAttributeProps'

import Attribute from './attribute'
import SharedAttribute from './shared'
import CrossDimensionalAttribute from './cross-dimensional'
import OptionalAttribute from './optional'
import DescriptiveAttribute from './descriptive'
import PointDebugger from './debugger'

export default class SvgAttribute extends React.Component<{
  attr: IAttribute
  style: IStyle
  graph: IGraph
}, {}> {

  private DEBUG = false

  render() {
    let attr = this.props.attr

    var props = {
      key: attr.id,
      attr: attr,
      style: this.props.style,
      graph: this.props.graph
    } as SvgAttributeProps

    if (this.DEBUG) {
      return (
        <PointDebugger {...attr.position} >
          {this.selectType(props)}
        </PointDebugger>)
    }

    return this.selectType(props)
  }

  selectType(props: SvgAttributeProps) {

    let modeling = props.attr.modeling

    if (modeling === undefined || modeling === {})
      return <Attribute {...props} />

    if (modeling.isDescriptive)
      return <DescriptiveAttribute {...props} />

    if (modeling.isOptional)
      return <OptionalAttribute {...props} />

    if (modeling.isShared)
      return <SharedAttribute {...props} />

    if (modeling.isCrossDimensional)
      return <CrossDimensionalAttribute {...props} />

    return <Attribute {...props} />
  }
}
