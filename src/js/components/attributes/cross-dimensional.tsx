import * as React from 'react'
import Attribute from './attribute'
import { IAttribute, IArc } from '../../model/interfaces'
import { RadiansAngle, DegreesAngle } from '../../model/angles'
import AngleCalculatorHelper from '../../helpers/angle-calculator-helper'
import GrapHelper from '../../helpers/graph-helper'

export default class CrossDimensionalAttribute extends Attribute {

  decorate(attr: IAttribute) {

    let graph = new GrapHelper(this.props.graph)
    let angleCalc = new AngleCalculatorHelper()

    let angles = graph.incomingArcs(attr).map(arc => {
      let { from, to } = graph.getEdgesAttributes(arc)
      return angleCalc.getAngleBetween(to.position, from.position).toDegrees().normalize()
    })

    let angle = angles.reduce(DegreesAngle.sum).value / angles.length

    let r = 14
    let path = `M ${attr.position.x},${attr.position.y} m ${-r},0 a ${r},${r} 0 1,0 ${r * 2},0`
    let rotate = `rotate(${angle - 90} ${attr.position.x} ${attr.position.y} )`

    return <path d={path} {...this.props.style.line} transform={rotate} />
  }
}