import { IStyle } from '../style'
import * as React from 'react'
import * as Dfm from '../../model/interfaces'
import { RenderMode } from '../../model/indyco'

export interface SvgAttributeProps {
  key: string
  attr: Dfm.IAttribute
  renderMode: RenderMode
  style: IStyle
  graph: Dfm.IGraph
}
