import * as React from 'react'

import Attribute from './attribute'
import { IAttribute } from '../../model/interfaces'

export default class SharedAttribute extends Attribute {

  decorate(attr: IAttribute) {

    var innerCircle = { cx: attr.position.x, cy: attr.position.y, r: 6 }

    return <circle {...innerCircle} {...this.props.style.line} />
  }
}