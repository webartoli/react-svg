///<reference path="./../../../../type-declarations/react-sizeme.d.ts"/>

import * as React from 'react'
import SizeMe from 'react-sizeme'
import { SvgAttributeProps } from './SvgAttributeProps'

export class DescriptiveAttribute extends React.Component<SvgAttributeProps, {}> {

  render() {
    let attr = this.props.attr
    const { width, height } = (this.props as any).size

    const textOffset = -5
    var underlineLenght = width - 1

    var underline = {
      x1: attr.position.x,
      y1: attr.position.y,
      x2: attr.position.x + underlineLenght,
      y2: attr.position.y
    }

    var opt = {} as any

    if (attr.id === "attr1") {
      opt.textAnchor = "end"
      underline.x1 -= underlineLenght
      underline.x2 -= underlineLenght
    }

    var style = this.props.style.line
    style.stroke = "#B4CEDA"

    return (
      <g>
        <text x={attr.position.x} y={attr.position.y + textOffset} {...this.props.style.label} {...opt}>{attr.name}</text>
        <line {...underline} {...this.props.style.line} />
      </g>
    )
  }
}

export default SizeMe()(DescriptiveAttribute)