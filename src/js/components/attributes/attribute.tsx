import * as React from 'react'
import ReactTooltip from 'react-tooltip'
import * as Dfm from '../../model/interfaces'
import { SvgAttributeProps } from './SvgAttributeProps'

export default class Attribute extends React.Component<SvgAttributeProps, {}> {

  render() {
    let attr = this.props.attr
    var position = { cx: attr.position.x, cy: attr.position.y }

    return (
      <g>
        <circle {...position} {...this.props.style.attribute} />
        {this.decorate(attr)}
        {this.label(attr)}
      </g>
    )
  }

  decorate(attr: Dfm.IAttribute) {

  }

  label(attr: Dfm.IAttribute) {
    var labelOffset = { x: 18, y: 5 }
    var position = { x: attr.position.x + labelOffset.x, y: attr.position.y + labelOffset.y }
    return <text {...position} {...this.props.style.label}>{attr.name}</text>
  }
}