import { IGraph, IAttribute, IArc, INode } from './../model/interfaces'

export default class GraphHelper {

  constructor(private graph: IGraph) { }

  getAttributeById(id: string): IAttribute {
    return this.graph.attributes.find(x => x.id === id)
  }

  getEdgesAttributes(arc: IArc): { from: IAttribute, to: IAttribute } {
    return {
      from: this.getAttributeById(arc.from),
      to: this.getAttributeById(arc.to)
    }
  }

  incomingArcs(attribute: IAttribute): IArc[] {
    return this.graph.arcs.filter(x => x.to === attribute.id)
  }
}