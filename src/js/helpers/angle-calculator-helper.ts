
import { IPoint, IPolarPoint } from '../model/points'
import { RadiansAngle, Radians, Degrees } from '../model/angles'

export default class AngleCalculatorHelper {

  getAngleBetween(a: IPoint, b: IPoint): RadiansAngle {
    var rad = Math.atan2(b.y - a.y, b.x - a.x)
    return new RadiansAngle(rad)
  }
}