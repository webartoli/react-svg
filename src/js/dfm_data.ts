import { IArc, IGraph } from './model/interfaces'

let arcs = <IArc[]>[
  { id: "arc1", from: "attr1", to: "attr2" },
  { id: "arc2", from: "attr2", to: "attr3", role: "b" },
  { id: "arc3", from: "attr3", to: "attr4", role: "c" },
  { id: "arc4", from: "attr4", to: "attr5", modeling: { isOptional: true } },
  { id: "ar4b", from: "attr3", to: "attr5" },
  { id: "arc7", from: "attr4", to: "attr7" },
  { id: "ar7b", from: "attr3", to: "attr7" },
  { id: "arc5", from: "attr4", to: "attr6" },
]

export default {
  id: "",
  name: "",
  attributes: [
    { id: "attr1", name: "Left Descriptive", description: "abc", position: { x: 110, y: 20 }, modeling: { isDescriptive: true } },
    { id: "attr2", name: "Optional", description: "abc", position: { x: 150, y: 60 }, modeling: { isOptional: true }, fill: "orange" },
    { id: "attr3", name: "Shared", description: "abc", position: { x: 150, y: 100 }, modeling: { isShared: true }, fill: "green" },
    { id: "attr4", name: "Normal", description: "abc", position: { x: 150, y: 180 } },
    { id: "attr5", name: "Cross 1", description: "first", position: { x: 190, y: 140 }, modeling: { isCrossDimensional: true }, fill: "red" },
    { id: "attr6", name: "Right Descriptive", description: "abc", position: { x: 190, y: 220 }, modeling: { isDescriptive: true } },
    { id: "attr7", name: "Cross 2", description: "first", position: { x: 110, y: 140 }, modeling: { isCrossDimensional: true }, fill: "red" },
  ],
  arcs: arcs,
  facts: []
} as IGraph