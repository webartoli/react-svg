
export interface IPoint {
  x: number
  y: number
}

export interface IPolarPoint {
  r: number
  q: number
}

export class Point implements IPoint {

  constructor(public x: number, public y: number) {

  }

  static center(from: IPoint, to: IPoint): IPoint {
    return {
      x: from.x + ((to.x - from.x) / 2),
      y: from.y + ((to.y - from.y) / 2)
    }
  }

  toPolar(): PolarPoint {
    return new PolarPoint(
      Math.sqrt((this.x * this.x) + (this.y * this.y)),
      Math.atan2(this.y, this.x))
  }
}

export class PolarPoint implements IPolarPoint {

  constructor(public r: number, public q: number) {
  }

  toCartesian(): Point {
    return new Point(
      this.r * Math.cos(this.q),
      this.r * Math.sin(this.q))
  }
}
