import { IPoint } from './points'


export interface IDfmElement {
  id: string
  name: string
  description: string
}

export interface INode {
  position: IPoint
}

export interface IAttribute extends IDfmElement, INode {
  modeling?: {
    isCrossDimensional?: boolean
    isShared?: boolean
    isOptional?: boolean
    isDescriptive?: boolean
  }

  fill?: string
}

export type NodeId = string

export interface IFact extends IDfmElement, INode {
  measures: IMeasure[]
}

export interface IArc {
  id: string
  role?: string
  from: NodeId
  to: NodeId
  modeling?: {
    isMultiple?: boolean
    isOptional?: boolean
    isRecursive?: boolean
    isConvergence?: boolean
  }
}

export interface IMeasure extends IDfmElement {

}

export interface IGraph extends IDfmElement {
  attributes: IAttribute[]
  arcs: IArc[]
  facts: IFact[]
}
