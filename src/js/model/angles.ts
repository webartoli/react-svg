
export type Degrees = number
export type Radians = number

export class DegreesAngle {

  constructor(public value: Degrees) {

  }

  toRadians(angle: Degrees): RadiansAngle {
    return new RadiansAngle(angle * Math.PI / 180)
  }

  normalize(): DegreesAngle {
    var offset = 90
    var circle = 360
    this.value = ((this.value + offset + circle) % circle) - offset
    return this
  }

  static min(first: DegreesAngle, second: DegreesAngle): DegreesAngle {
    return first.value < second.value ? first : second
  }

  static max(first: DegreesAngle, second: DegreesAngle): DegreesAngle {
    return first.value > second.value ? first : second
  }

  static sum(first: DegreesAngle, second: DegreesAngle): DegreesAngle {
    return new DegreesAngle(first.value + second.value)
  }
}

export class RadiansAngle {

  constructor(public value: Radians) {

  }

  toDegrees(): DegreesAngle {
    return new DegreesAngle(180 * this.value / Math.PI)
  }
}