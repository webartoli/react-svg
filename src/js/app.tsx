import * as React from 'react'
import DfmGraph from './components/graph'
import * as Dfm from './model/interfaces'
import { RenderMode } from './model/indyco'

interface AppProps {
  dfmGraph: Dfm.IGraph
}

export default class App extends React.Component<AppProps, {}> {

  render() {

    var renderMode = RenderMode.Builder

    return (
      <div>
        <DfmGraph graph={this.props.dfmGraph} renderMode={renderMode} />
      </div>
    )
  }
}